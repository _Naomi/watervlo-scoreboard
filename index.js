require('dotenv').config()

// Import modules.
const bcrypt = require('bcrypt')
const express = require('express')
const fs = require('fs')
const helmet = require('helmet')
const http = require('http')
const path = require('path')
const session = require('express-session')
const FileStore = require('session-file-store')(session)
const WebSocket = require('ws')
const CronJob = require('cron').CronJob

// Create directories if not exists.
if (!fs.existsSync('./logs')) {
  fs.mkdirSync('./logs')
}
if (!fs.existsSync('./data')) {
  fs.mkdirSync('./data')
}

// Load the logger
const log = require('./utils/logger.js').logger
const closeLog = require('./utils/logger.js').closeLogger

// Load the users data.
const users = require('./utils/global_db.js').usersData // TODO Find better way to do this?

// Configure express.
const app = express()
app.enable('trust proxy')
app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(helmet({
  contentSecurityPolicy: false, // TODO Find out how to fix this
  frameguard: false // TODO Is this safe?
}))

const sessionParser = session({
  store: new FileStore(),
  name: 'user_sid',
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: false,
  cookie: {
    sameSite: 'strict',
    maxAge: 31536000000
  }
})

app.use(sessionParser)

// Configure routes
// TODO Temp, find permanent solution
app.locals.colors = {}
app.locals.colors.watervlo_yellow = '#FFEC2D'
app.locals.colors.watervlo_blue = '#0057B8'
app.locals.colors.home_background = '#FDD835'
app.locals.colors.away_background = '#1A237E'
app.locals.colors.home_text = app.locals.colors.away_background
app.locals.colors.away_text = app.locals.colors.home_background
app.locals.colors.reset_background = '#f44336'
app.locals.colors.reset_text = app.locals.colors.watervlo_yellow

// Root
app.get('/', (req, res) => {
  res.redirect('controlepaneel')
})

// Login
app.route('/login')
  .get((req, res) => {
    res.render('login', { env: process.env })
  })
  .post((req, res) => {
    const username = req.body.username === undefined ? undefined : req.body.username.toLowerCase()
    const password = req.body.password

    if (users[username] === undefined) {
      return res.redirect('login?error=Ongeldige+gebruikersnaam')
    }

    bcrypt.compare(password, users[username].password, (error, match) => {
      if (error) throw error
      if (match) {
        req.session.user = username

        if (users[username].resetPassword) {
          return res.redirect('resetPassword')
        }
        res.redirect('controlepaneel')
      } else {
        res.redirect('login?error=Ongeldig+wachtwoord')
      }
    })
  })

// Logout
app.get('/logout', (req, res) => {
  if (req.session.user) {
    req.session.destroy()
  }
  res.redirect('login')
})

// Reset password
app.route('/resetPassword')
  .get((req, res) => {
    if (req.session.user && req.session.user !== 'scoreboard' && req.session.user !== 'shotclock') {
      res.render('resetPassword', { env: process.env })
    } else {
      res.redirect('login')
    }
  })
  .post((req, res) => {
    log.info('Password reset requested.')

    const password1 = req.body.password1
    const password2 = req.body.password2

    if (password1 !== password2) {
      return res.redirect('resetPassword?error=Wachtwoord+komen+niet+overeen')
    }
    if (!password1) {
      return res.redirect('resetPassword?error=Ongeldig+wachtwoord')
    }

    bcrypt.hash(password1, 10, (err, hash) => {
      if (err) {
        log.error('Error occurred while hashing password')
        return res.redirect('resetPassword?error=Er+ging+iets+fout,+probeer+opnieuw')
      }
      users[req.session.user].password = hash
      users[req.session.user].resetPassword = false

      res.redirect('controlepaneel')

      log.info(`${req.session.user} successfully reset their password.`)
    })
  })

// Controlpanel
app.get('/controlepaneel', (req, res) => {
  if (req.session.user && req.session.user !== 'scoreboard' && req.session.user !== 'shotclock') {
    if (users[req.session.user].resetPassword) {
      return res.redirect('resetPassword')
    }

    users[req.session.user].lastOnline = Date.now()

    fs.readdir(path.join(__dirname, '/views/controlpanel_pages'), (err, files) => {
      if (err) throw err
      res.render('controlpanel', { files, env: process.env })
    })
  } else {
    res.redirect('login')
  }
})

// Scoreboard
app.get('/scorebord', (req, res) => {
  if (req.session.user === undefined) {
    req.session.user = 'scoreboard'
  }
  fs.readdir(path.join(__dirname, '/views/scoreboard_pages'), (err, files) => {
    if (err) throw err
    res.render('scoreboard', { files })
  })
})

// Shotclock
app.get('/schotklok', (req, res) => {
  if (req.session.user === undefined) {
    req.session.user = 'shotclock'
  }
  fs.readdir(path.join(__dirname, '/views/shotclock_pages'), (err, files) => {
    if (err) throw err
    res.render('shotclock', { files })
  })
})

// Media
app.use('/media/', express.static(path.join(__dirname, '/media')))

// CSS
app.use('/css/', express.static(path.join(__dirname, '/css')))

// Favicon
app.use('/', express.static(path.join(__dirname, '/favicon')))

// TextFit
app.get('/textFit.min.js', (req, res) => {
  res.sendFile(path.join(__dirname, '/node_modules/textfit/textFit.min.js'))
})

// MaterializeCSS
app.get('/materialize.min.js', (req, res) => {
  res.sendFile(path.join(__dirname, '/node_modules/@materializecss/materialize/dist/js/materialize.min.js'))
})

// simplepeer
app.get('/simplepeer.min.js', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/simplepeer.min.js'))
})

// Setup HTTP server and websocket.
const httpServer = http.createServer(app)
const wss = new WebSocket.Server({ clientTracking: true, noServer: true })

httpServer.on('upgrade', (request, socket, head) => {
  log.info('Incomming HTTP connection, parsing session...')

  sessionParser(request, {}, () => {
    if (!request.session.user) {
      socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n')
      socket.destroy()
      return
    }

    log.info('Session is parsed.')

    wss.handleUpgrade(request, socket, head, (ws) => {
      wss.emit('connection', ws, request)
    })
  })
})

httpServer.listen(process.env.SCOREBOARD_HTTP_PORT, () => {
  log.info(`Started HTTP server on port ${process.env.SCOREBOARD_HTTP_PORT}.`)
})

const commands = {}

// Setup websocket.
wss.on('connection', (ws, request) => {
  const user = request.session.user
  ws.isAlive = true

  ws.sendCommand = (scope, command, data = {}) => {
    ws.send(JSON.stringify({ scope, command, data }))
  }

  if (user === 'scoreboard' || user === 'shotclock') {
    ws.clientType = user
  } else {
    ws.clientType = 'controlpanel'
  }

  Object.keys(commands).forEach((scope) => {
    commands[scope].initialCommands.forEach((command) => {
      command(ws)
    })
  })

  ws.on('message', (message) => {
    let data
    try {
      data = JSON.parse(message)
    } catch (error) {
      log.error(`Received invalid data from the client: ${message}`)
      return
    }
    if (user === 'scoreboard' || user === 'shotclock') {
      if (data.scope !== 'global' || data.command !== 'webrtc') {
        log.warn(`${user} tried to send a message, which shouldn't happend.`)
        return
      }
    }
    if (data.scope === undefined || data.command === undefined) {
      log.warn(`Did not receive command from client: ${data}`)
      return
    } else if (commands[data.scope] === undefined || commands[data.scope][data.command] === undefined) {
      log.warn('Received command that isn\'t registered: ' + JSON.stringify(data))
      return
    }

    log.debug(`Called the command '${data.command}' in scope '${data.scope}' with data ${JSON.stringify(data.data)}`)
    commands[data.scope][data.command](ws, data.data)
  })

  ws.on('close', () => {
    log.info('Closed connection') // TODO: Add more information like which connection?
  })
  ws.on('pong', () => {
    ws.isAlive = true
  })
})

// Websocket heartbeat
const wsHeartbeatInterval = setInterval(() => {
  wss.clients.forEach((ws) => {
    if (ws.isAlive === false) return ws.terminate()

    ws.isAlive = false
    ws.ping()
  })
}, 30 * 1000)
wss.on('close', () => {
  clearInterval(wsHeartbeatInterval)
})

// Create command system
function sendCommand (scope, command, data = {}, controlpanel = true, scoreboard = true, shotclock = true) {
  wss.clients.forEach((client) => {
    if ((client.clientType === 'controlpanel' && controlpanel) ||
        (client.clientType === 'scoreboard' && scoreboard) ||
        (client.clientType === 'shotclock' && shotclock)) {
      client.sendCommand(scope, command, data)
    }
  })
}

function registerCommand (scope, command, func, sendOnConnect = false) {
  if (commands[scope] === undefined) {
    commands[scope] = {}
    commands[scope].initialCommands = []
  }
  if (commands[scope][command] !== undefined) {
    throw new Error(`The command ${command} is already registered in scope ${scope}!`)
  }
  if (command === 'initialCommands') {
    throw new Error('You can have a command nammed \'initialCommands\'!')
  }
  commands[scope][command] = func

  if (sendOnConnect) {
    commands[scope].initialCommands.push(func)
  }
}

// Load all actions
const actions = fs.existsSync('./data/actions.json') ? JSON.parse(fs.readFileSync('./data/actions.json', 'utf8')) : []
const jobs = []
// Verify actions
actions.forEach((action) => {
  if (!['cron', 'action'].every(key => Object.keys(action).includes(key))) throw new Error('Impropperly formatted actions.json')
  jobs.push(new CronJob(
    action.cron, // cronTime
    () => { eval(action.action) }, // onTick // eslint-disable-line no-eval
    null, // onComplete
    true // start
  ))
})

// Load all the scopes.
const scopes = require('./utils/scope_loader.js')
scopes.load(registerCommand, sendCommand)

// Gracefully shut down the server.
function shutdown () {
  log.info('Gracefully shutting down')

  // Save all the data.
  scopes.close()

  // Save all the global databases.
  require('./utils/global_db.js').close()

  log.info('Closing HTTP server')
  httpServer.close()

  log.info('Closing websocket server')
  wss.close()
  wss.clients.forEach((client) => {
    client.close()
  })

  log.info('Closing logger')
  closeLog()
}

process.on('SIGINT', shutdown)
process.on('SIGTERM', shutdown)
