# Scoreboard for Watervlo

## Setup

First clone this repository using the following command:
```
git clone https://gitlab.com/_Naomi/watervlo-scoreboard.git
```

Then go inside the created folder and run the following command:
```
npm install
```

Then you need to setup a basic `.env` file for your environment.
```
NODE_ENV=production
SCOREBOARD_HTTP_PORT=3000
SESSION_SECRET=SomeSecret
SPORTLINK_WATERVLO_ID=ID
FOOTBALL_DATA_API_KEY=KEY
SPONSOR_TEAMS=Comma-separated list of local team IDs
```

You can then start the application by running:
```
npm start
```

## Usefull information

### How do the commands work?

The server and clients communicate with each other via JSON messages.  
The JSON object should have at least 2 parameters, 1 named scope and 1 named command.  
The scope is the scope you want to send the command to. There is 1 `global` scope, and then every page is it's own scope.  
Then there is a third and optional argument, the data argument. If you have this argument, it should be an object containing all the relevant data.  
You also need to make sure that the JSON is valid and can be parsed. The easiest way is to stringify an object before you send it.

```
ws.send(JSON.stringify({ scope: 'global', command: 'setPage', data: { page: 'start' } }))
```

Messages send from the scoreboard and shotclock client are ignored, and thus should't happen. (Because those clients aren't authenticated)  
This means that every time you update some data, you have to send out that new data to every client.

### Adding a new page

To add a new page, you have to do a few things, depending on what you need.

If you want your page to store data, make a new JsonDB object near the bottom in `index.js`  
This is also where you should put the server side commands. Register a new command with `registerCommand(scope, command, funcToExecute)`  
The `funcToExecute` will receive 2 parameters: A WebSocket object that is the websocket that send the command, and an object with the optional data.

Then, you can create a page for each client type in `views/<clientType>_pages/<page>.ejs`  
You don't have to add a page for eacht client type, as it will default to `start` if there is none.  
In this file you can put your CSS, HTML and Javascript.  
If one of these sections is not needed for your page, you can just delete that section from your file.  

```html
<% if (section === 'css') { %>
<style>
</style>
<% } %>

<% if (section === 'html') { %>
<div class="page" page="pageName">
</div>
<% } %>

<% if (section === 'js') { %>
<script>
</script>
<% } %>
```

In these script tags is where you should register your client side commands, also with `registerCommand(scope, command, funcToExecute)`  
Here however, the only argument given to the `funcToExecute` is the optional data.  
If you want to send something to the server you should use `connection.send()`

You also need to remember to put your new page in the navigation menus.  
There is a navbar for desktop clients, and a sidenav for mobile clients.  
You need to add your page in both these menus.

### Playing a sound

There are 3 sounds/buzzers that you can play. A normal buzzer, one for the end of a perdiod and one for the end of a shotclock.  
You can play them with the following functions in any of the scoreboard pages:  
`playBuzzer(int, bool)`  
`playEndPeriod(bool)`  
`playEndShotclock(bool)`  

They all accept a boolean which determine whether or not to also show the visual buzzer. (Default true)  
The buzzer also accepts an int, which is the number of times to play the buzzer. (Default 1)

### Compiling css

If you changed something with Materializecss and want to update the css files, run the following commands:

```
npx sass node_modules/materialize-css/sass/materialize.scss css/materialize.css --no-source-map --style expanded
npx sass node_modules/materialize-css/sass/materialize.scss css/materialize.min.css --no-source-map --style compressed
```
