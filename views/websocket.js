let timeOffset = 0
let pages
let startPage

document.addEventListener('DOMContentLoaded', () => {
  pages = document.querySelectorAll('.page[page]')
  startPage = document.querySelector('.page[page=start]')

  if (startPage === null) {
    console.error('No start page found, this should never happen!')
  }
})

function changePage (pageName) {
  console.log(`Changing page to ${pageName}`)
  let notFound = true

  for (const page of pages) {
    if (page.getAttribute('page') === pageName) {
      page.style.visibility = 'visible'
      notFound = false
    } else {
      page.style.visibility = 'hidden'
    }

    // If the page has subpages, make the active one visible if the page is also visible.
    if (page.getAttribute('subpage') !== null) {
      for (const subpage of page.querySelectorAll('.subpage[subpage]')) {
        if (page.getAttribute('page') === pageName && subpage.getAttribute('subpage') === page.getAttribute('subpage')) {
          subpage.style.visibility = 'visible'
        } else {
          subpage.style.visibility = 'hidden'
        }
      }
    }
  }

  if (notFound) {
    startPage.style.visibility = 'visible'
  }
}

function changeSubPage (pageName, subPageName) {
  for (const page of pages) {
    // Change the subpage attribute to active subpage.
    if (page.getAttribute('page') === pageName) {
      page.setAttribute('subpage', subPageName)

      // If the page is active, call changePage to update it.
      if (page.style.visibility === 'visible') {
        changePage(pageName)
      }
    }
  }
}

const commands = {}

function registerCommand (scope, command, func) {
  if (commands[scope] === undefined) {
    commands[scope] = {}
  }
  commands[scope][command] = func
}

let connection
let connectionClosed = false

function openWS () {
  console.log('Opening websocket')
  if (location.protocol === 'http:') {
    connection = new WebSocket(`ws://${location.host + location.pathname.split('/').slice(0, -1).join('/')}/`)
  } else {
    connection = new WebSocket(`wss://${location.host + location.pathname.split('/').slice(0, -1).join('/')}/`)
  }

  connection.sendCommand = (scope, command, data = {}) => {
    connection.send(JSON.stringify({ scope, command, data }))
  }

  connection.addEventListener('open', (event) => {
    console.log('Websocked opened')

    if (connectionClosed && typeof M !== 'undefined') {
      M.toast({ html: 'Verbinding met de server herstelt' })
      connectionClosed = false
    }
  })

  connection.addEventListener('message', (event) => {
    let message
    try {
      message = JSON.parse(event.data)
    } catch (error) {
      console.error(`Could not parse message: ${event.data}`)
      return
    }
    console.log('Received data from the server', message)

    if (commands[message.scope] === undefined) {
      return
    }
    if (commands[message.scope][message.command] === undefined) {
      return
    }
    commands[message.scope][message.command](message.data)
  })

  connection.addEventListener('close', (event) => {
    console.warn('Connection died, trying to reconnect in 10 seconds...')
    setTimeout(openWS, 10000)

    if (typeof M !== 'undefined') {
      M.toast({ html: 'Verbinding met de server verbroken' })
      connectionClosed = true
    }
  })

  connection.addEventListener('error', (event) => {
    console.error('The websocket encountered an error: ', event)
    connection.close()
  })
}

openWS()

registerCommand('global', 'refresh', () => {
  location.reload()
})

registerCommand('global', 'syncTime', (data) => {
  timeOffset = (data.time - Date.now())
  console.log(`Time offset is: ${timeOffset}`)
})
