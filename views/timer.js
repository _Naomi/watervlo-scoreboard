class Timer {
  constructor (intervalTime, direction, setElement, shouldTick = null) {
    this.intervalTime = intervalTime
    this.direction = direction
    this.setElement = setElement
    this.shouldTick = shouldTick

    this.isRunning = false
  }

  setTime (time) {
    this.time = time
    this.setElement(time)
  }

  start () {
    if (this.isRunning) {
      return
    }
    let startTime = Date.now()
    this.isRunning = true
    this.interval = setInterval(() => {
      if (this.time <= 0 && this.direction === 'down') { // Decrement
        this.time = 0
        this.setElement(this.time)
        this.stop()
        return
      }

      const delta = Date.now() - startTime
      if (delta >= this.intervalTime) {
        startTime += this.intervalTime
        if (this.shouldTick === null || this.shouldTick(this.time)) {
          if (this.direction === 'down') { // Decrement
            this.time -= (this.intervalTime / 1000)
          } else { // Increment
            this.time += (this.intervalTime / 1000)
          }
          this.setElement(this.time)
        }
      }
    }, this.intervalTime / 10)
  }

  stop () {
    clearInterval(this.interval)
    this.isRunning = false
  }
}
