const fs = require('fs')
const CronJob = require('cron').CronJob

function replaceObject (original, replace) {
  Object.keys(replace).forEach((key) => {
    original[key] = replace[key]
  })
}

class JsonDB {
  constructor (filepath, obj = {}, autosave = 600, resetAtMidnight = false) {
    this.filepath = filepath
    this.default = obj
    this.obj = fs.existsSync(filepath) ? require(filepath) : obj
    this.autosave = autosave
    this.modified = false

    this.save = () => {
      fs.writeFileSync(this.filepath, JSON.stringify(this.obj, null, 4))
    }

    this.close = () => {
      clearInterval(this.interval)
      this.save()
    }

    if (this.autosave != null) {
      this.interval = setInterval(() => {
        if (this.modified) {
          this.save()
          this.modified = false
        }
      }, this.autosave * 1000)
    }

    if (resetAtMidnight) {
      this.job = new CronJob('0 0 0 * * *', () => {
        replaceObject(this.obj, this.default)
      }, null, true)
    }

    return new Proxy(this.obj, {
      set: (target, name, value) => {
        const nonSetables = ['save', 'close']
        if (nonSetables.includes(name)) {
          throw new Error(`Cannot set the ${name} property.`)
        } else {
          target[name] = value
          this.modified = true
          return true
        }
      },
      get: (target, property) => {
        if (property === 'save') {
          return this.save
        } else if (property === 'close') {
          return this.close
        }
        return target[property]
      },
      ownKeys: (target) => {
        return Object.keys(target)
      }
    })
  }
}

module.exports = JsonDB
