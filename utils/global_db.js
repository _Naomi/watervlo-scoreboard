const JsonDB = require('./json_db.js')
const path = require('path')

module.exports.usersData = new JsonDB(path.join(__dirname, '../data/users.json'), {}, 600)

module.exports.teamsData = new JsonDB(path.join(__dirname, '../data/teams.json'), {}, 600)

module.exports.trainingsData = new JsonDB(path.join(__dirname, '../data/trainings.json'), {}, 600)

module.exports.close = () => {
  module.exports.usersData.close()
  module.exports.teamsData.close()
  module.exports.trainingsData.close()
}
