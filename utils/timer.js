class Timer {
  constructor (intervalTime, setPoint = 0, loop = false, direction = 'down', onEnd = () => {}, shouldTick = null) {
    this.intervalTime = intervalTime
    this.setPoint = setPoint
    this.loop = loop
    this.direction = direction
    this.onEnd = onEnd
    this.shouldTick = shouldTick

    if (this.direction === 'down') { // Decrement
      this.time = setPoint
    } else { // Increment
      this.time = 0
    }
    this.isRunning = false
  }

  setTime (time) {
    this.time = time
  }

  start () {
    if (this.running) {
      return
    }
    if (this.setPoint === 0) {
      return
    }
    let startTime = Date.now()
    this.isRunning = true
    this.interval = setInterval(() => {
      if (this.setPoint === 0) {
        this.stop()
        this.onEnd()
        return
      } else if (this.time <= 0 && this.direction === 'down') { // Decrement
        this.time = this.setPoint
        if (!this.loop) {
          this.stop()
        }
        this.onEnd()
        return
      } else if (this.time >= this.setPoint && this.direction !== 'down') { // Increment
        this.time = 0
        if (!this.loop) {
          this.stop()
        }
        this.onEnd()
        return
      }

      const delta = Date.now() - startTime
      if (delta >= this.intervalTime) {
        startTime += this.intervalTime
        if (this.shouldTick === null || this.shouldTick(this.time)) {
          if (this.direction === 'down') { // Decrement
            this.time -= (this.intervalTime / 1000)
          } else { // Increment
            this.time += (this.intervalTime / 1000)
          }
        }
      }
    }, this.intervalTime / 10)
  }

  stop () {
    clearInterval(this.interval)
    this.isRunning = false
  }
}

module.exports = Timer
