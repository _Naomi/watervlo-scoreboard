const fs = require('fs')
const path = require('path')

const scopes = {}

module.exports.load = (registerCommand, sendCommand) => {
  fs.readdir(path.join(__dirname, '../scopes/'), (err, files) => {
    if (err) throw err
    for (const file of files) {
      const name = file.slice(0, -3)

      scopes[name] = require(path.join(__dirname, '../scopes/', file))
      scopes[name].registerCommands(registerCommand, sendCommand)
    }
  })
}

module.exports.close = () => {
  Object.values(scopes).forEach((scope) => {
    scope.close()
  })
}
