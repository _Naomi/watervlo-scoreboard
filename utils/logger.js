const manager = require('simple-node-logger').createLogManager()
const path = require('path')

// Create the logger.
manager.createRollingFileAppender({
  errorEventName: 'error',
  logDirectory: path.join(__dirname, '../logs'),
  fileNamePattern: 'log-<DATE>.log',
  dateFormat: 'YYYY-MM-DD'
})
const log = manager.createLogger()
log.info('Initialized logger.')

// Temporary function to close the logger.
function closeLogger () {
  (manager.getAppenders()).forEach(appender => {
    if (appender.__protected) {
      const rollTimer = appender.__protected().rollTimer
      if (rollTimer) {
        clearInterval(rollTimer)
      }
    }
  })
}

module.exports.logger = log
module.exports.closeLogger = closeLogger
