const path = require('path')
const JsonDB = require('../utils/json_db.js')
const Timer = require('../utils/timer.js')

const scoreboardData = new JsonDB(path.join(__dirname, '../data/scoreboard.json'), {
  home: 0,
  away: 0,
  period: 1,
  periodTime: 10
}, 600, true)

module.exports.registerCommands = (registerCommand, sendCommand) => {
  const shotclockTimer = new Timer(100, 30, false, 'down', () => {
    sendCommand('scoreboard', 'shotclockEnded')
    sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: false })
  })

  const periodclockTimer = new Timer(1000, scoreboardData.periodTime * 60, false, 'down', () => {
    scoreboardData.period++
    sendCommand('scoreboard', 'periodclockEnded')
    sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: false })
    sendCommand('scoreboard', 'data', scoreboardData)
  },
  (time) => {
    return time > shotclockTimer.time
  })

  registerCommand('scoreboard', 'data', (ws) => {
    ws.sendCommand('scoreboard', 'data', scoreboardData)
  }, true)

  registerCommand('scoreboard', 'resetAll', () => {
    scoreboardData.home = 0
    scoreboardData.away = 0
    scoreboardData.period = 1

    sendCommand('scoreboard', 'data', scoreboardData)

    periodclockTimer.stop()
    periodclockTimer.setTime(scoreboardData.periodTime * 60)
    sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: false })

    shotclockTimer.stop()
    shotclockTimer.setTime(30)
    sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: false })
  })

  registerCommand('scoreboard', 'homePlus', () => {
    scoreboardData.home++
    sendCommand('scoreboard', 'data', scoreboardData)
  })

  registerCommand('scoreboard', 'homeMin', () => {
    if (scoreboardData.home > 0) {
      scoreboardData.home--
      sendCommand('scoreboard', 'data', scoreboardData)
    }
  })

  registerCommand('scoreboard', 'awayPlus', () => {
    scoreboardData.away++
    sendCommand('scoreboard', 'data', scoreboardData)
  })

  registerCommand('scoreboard', 'awayMin', () => {
    if (scoreboardData.away > 0) {
      scoreboardData.away--
      sendCommand('scoreboard', 'data', scoreboardData)
    }
  })

  registerCommand('scoreboard', 'periodPlus', () => {
    scoreboardData.period++
    sendCommand('scoreboard', 'data', scoreboardData)
  })

  registerCommand('scoreboard', 'periodMin', () => {
    if (scoreboardData.period > 1) {
      scoreboardData.period--
      sendCommand('scoreboard', 'data', scoreboardData)
    }
  })

  registerCommand('scoreboard', 'periodclockStart', () => {
    periodclockTimer.start()
    sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: true })
  })

  registerCommand('scoreboard', 'periodclockStop', () => {
    periodclockTimer.stop()
    sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: false })
  })

  registerCommand('scoreboard', 'periodclockToggle', () => {
    if (periodclockTimer.isRunning) {
      periodclockTimer.stop()
    } else {
      periodclockTimer.start()
    }
    sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: periodclockTimer.isRunning })
  })

  registerCommand('scoreboard', 'periodclockGetTime', (ws) => {
    ws.sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: periodclockTimer.isRunning })
  }, true)

  registerCommand('scoreboard', 'periodclockSetTime', (ws, data) => {
    scoreboardData.periodTime = data.time
    periodclockTimer.setTime(data.time * 60)
    periodclockTimer.setPoint = data.time * 60
    sendCommand('scoreboard', 'periodclockTime', { time: periodclockTimer.time, running: periodclockTimer.isRunning })
  })

  registerCommand('scoreboard', 'shotclockStart', () => {
    shotclockTimer.start()
    sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: true })
  })

  registerCommand('scoreboard', 'shotclockStop', () => {
    shotclockTimer.stop()
    sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: false })
  })

  registerCommand('scoreboard', 'shotclockToggle', () => {
    if (shotclockTimer.isRunning) {
      shotclockTimer.stop()
    } else {
      shotclockTimer.start()
    }
    sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  })

  registerCommand('scoreboard', 'shotclockGetTime', (ws) => {
    ws.sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  }, true)

  registerCommand('scoreboard', 'shotclockSetTime', (ws, data) => {
    if (data.time < periodclockTimer.time) {
      shotclockTimer.setTime(data.time)
    } else {
      shotclockTimer.setTime(periodclockTimer.time)
    }
    sendCommand('scoreboard', 'shotclockTime', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  })
}

module.exports.close = () => {
  scoreboardData.close()
}
