const path = require('path')
const JsonDB = require('../utils/json_db.js')
const log = require('../utils/logger.js').logger

const globalData = new JsonDB(path.join(__dirname, '../data/global.json'), {
  page: 'start',
  activeTeam: ''
}, 600, true)

module.exports.registerCommands = (registerCommand, sendCommand) => {
  registerCommand('global', 'data', (ws) => {
    ws.sendCommand('global', 'data', globalData)
  }, true)

  registerCommand('global', 'setPage', (_, data) => {
    globalData.page = data.page
    log.info(`Changed page to '${data.page}'`)
    sendCommand('global', 'data', globalData)
  })

  registerCommand('global', 'syncTime', (ws) => {
    ws.sendCommand('global', 'syncTime', { time: Date.now() })
  }, true)

  registerCommand('global', 'refresh', () => {
    log.info('Refresh all the active scoreboards and shotclocks.')
    sendCommand('global', 'refresh', {}, false, true, true)
  })

  registerCommand('global', 'webrtc', (ws, data) => {
    const targetScoreboard = data.target === 'scoreboard'
    const targetControlpanel = data.target === 'controlpanel'
    sendCommand('global', 'webrtc', data, targetControlpanel, targetScoreboard, false)
  })
}

module.exports.close = () => {
  globalData.close()
}
