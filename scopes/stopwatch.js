const Timer = require('../utils/timer.js')

module.exports.registerCommands = (registerCommand, sendCommand) => {
  const stopwatchTimer = new Timer(100, Infinity, false, 'up', () => {})

  function getStopwatchData () {
    return { time: stopwatchTimer.time, running: stopwatchTimer.isRunning }
  }

  registerCommand('stopwatch', 'time', (ws) => {
    ws.sendCommand('stopwatch', 'time', getStopwatchData())
  }, true)

  registerCommand('stopwatch', 'toggleTime', () => {
    if (stopwatchTimer.isRunning) {
      stopwatchTimer.stop()
    } else {
      stopwatchTimer.start()
    }

    sendCommand('stopwatch', 'time', getStopwatchData())
  })

  registerCommand('stopwatch', 'reset', () => {
    stopwatchTimer.stop()
    stopwatchTimer.setTime(0)

    sendCommand('stopwatch', 'time', getStopwatchData())
  })
}

module.exports.close = () => {}
