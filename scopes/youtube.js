const log = require('../utils/logger.js').logger

module.exports.registerCommands = (registerCommand, sendCommand) => {
  const youtubeRegEx = RegExp(/^(?:(?:https?:)?\/\/)?(?:(?:www|m)\.)?(?:youtube\.com|youtu.be)(?:\/(?:[\w-]+\?v=|embed\/|v\/)?)([\w-]+)(?:\S+)?$/)

  registerCommand('youtube', 'setVideo', (_, data) => {
    let youtubeId
    try {
      youtubeId = youtubeRegEx.exec(data.URL)[1]
    } catch (error) {
      log.error('Error occured while parsing a Youtube URL: ' + error)
    }
    if (youtubeId != null) {
      sendCommand('youtube', 'setVideo', { id: youtubeId }, false, true, false)
    } else {
      log.warn('Received invalid Youtube URL. URL: ' + data.URL + ' Id: ' + youtubeId)
    }
  })

  registerCommand('youtube', 'fullscreen', (_, data) => {
    sendCommand('youtube', 'fullscreen', data, false, true, false)
  })

  registerCommand('youtube', 'start', () => {
    sendCommand('youtube', 'start', {}, false, true, false)
  })

  registerCommand('youtube', 'pause', () => {
    sendCommand('youtube', 'pause', {}, false, true, false)
  })

  registerCommand('youtube', 'stop', () => {
    sendCommand('youtube', 'stop', {}, false, true, false)
  })

  registerCommand('youtube', 'plus10sec', () => {
    sendCommand('youtube', 'plus10sec', {}, false, true, false)
  })

  registerCommand('youtube', 'min10sec', () => {
    sendCommand('youtube', 'min10sec', {}, false, true, false)
  })
}

module.exports.close = () => {
}
