const path = require('path')
const JsonDB = require('../utils/json_db.js')
const Timer = require('../utils/timer.js')

const timerData = new JsonDB(path.join(__dirname, '../data/timer.json'), {
  setPoint: 0,
  loop: false,
  sound: false,
  direction: 'down'
}, 600)

module.exports.registerCommands = (registerCommand, sendCommand) => {
  const timerTimer = new Timer(1000, timerData.setPoint, timerData.loop, timerData.direction, () => {
    sendCommand('timer', 'timerEnded')

    sendCommand('timer', 'time', { time: timerTimer.time, running: timerTimer.isRunning })
  })

  function resetTimer () {
    timerTimer.setTime(timerData.direction === 'down' ? timerData.setPoint : 0)
  }

  registerCommand('timer', 'data', (ws) => {
    ws.sendCommand('timer', 'data', timerData)
  }, true)

  registerCommand('timer', 'time', (ws) => {
    ws.sendCommand('timer', 'time', { time: timerTimer.time, running: timerTimer.isRunning })
  }, true)

  registerCommand('timer', 'timeAdd', (_, data) => {
    timerData.setPoint += data.time
    timerTimer.setPoint = timerData.setPoint

    sendCommand('timer', 'data', timerData)
  })

  registerCommand('timer', 'timeSub', (_, data) => {
    timerData.setPoint -= data.time
    if (timerData.setPoint < 0) {
      timerData.setPoint = 0
    }
    timerTimer.setPoint = timerData.setPoint

    sendCommand('timer', 'data', timerData)

    if (timerTimer.time > timerData.setPoint) {
      timerTimer.setTime(timerData.setPoint)
      sendCommand('timer', 'time', { time: timerTimer.time, running: timerTimer.isRunning })
    }
  })

  registerCommand('timer', 'toggleLoop', () => {
    timerData.loop = !timerData.loop
    timerTimer.loop = timerData.loop
    sendCommand('timer', 'data', timerData)
  })

  registerCommand('timer', 'toggleSound', () => {
    timerData.sound = !timerData.sound
    sendCommand('timer', 'data', timerData)
  })

  registerCommand('timer', 'toggleDirection', () => {
    timerData.direction = timerData.direction === 'down' ? 'up' : 'down'
    timerTimer.direction = timerData.direction
    sendCommand('timer', 'data', timerData)

    resetTimer()
    sendCommand('timer', 'time', { time: timerTimer.time, running: timerTimer.isRunning })
  })

  registerCommand('timer', 'toggleTime', () => {
    if (timerTimer.isRunning) {
      timerTimer.stop()
    } else {
      timerTimer.start()
    }

    sendCommand('timer', 'time', { time: timerTimer.time, running: timerTimer.isRunning })
  })
}

module.exports.close = () => {
  timerData.close()
}
