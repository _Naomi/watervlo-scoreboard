require('dotenv').config()

const path = require('path')
const axios = require('axios')

const JsonDB = require('../utils/json_db.js')
const log = require('../utils/logger.js').logger

const soccerData = new JsonDB(path.join(__dirname, '../data/soccer.json'), {
  matchId: ''
}, 600)

const headers = { 'X-Auth-Token': process.env.FOOTBALL_DATA_API_KEY }

let getMatchInfoTimeout
let matchCache

function getMatchInfo (sendCommand) {
  if (soccerData.matchId !== '') {
    axios.get('http://api.football-data.org/v2/matches/' + soccerData.matchId, { headers: headers })
      .then((response) => {
        const match = response.data.match

        matchCache = {
          homeName: match.homeTeam.name,
          awayName: match.awayTeam.name,
          homeScore: match.score.fullTime.homeTeam,
          awayScore: match.score.fullTime.awayTeam,
          status: match.status,
          lastUpdate: match.lastUpdated
        }

        sendCommand('soccer', 'matchData', matchCache, false, true, false)

        if (match.status === 'FINISHED') {
          soccerData.matchId = ''
          matchCache = undefined
          return
        }

        clearTimeout(getMatchInfoTimeout)
        getMatchInfoTimeout = setTimeout(getMatchInfo, 30000, sendCommand)
      })
      .catch((error) => {
        log.error(error)
      })
  }
}

module.exports.registerCommands = (registerCommand, sendCommand) => {
  registerCommand('soccer', 'getMatches', (ws) => {
    axios.get('http://api.football-data.org/v2/matches', {
      params: {
        competitions: '2001,2003,2018'
      },
      headers: headers
    })
      .then((response) => {
        const matches = response.data.matches
        const filteredMatches = []

        for (const match of matches) {
          filteredMatches.push({
            id: match.id,
            home: match.homeTeam.name,
            away: match.awayTeam.name,
            competition: match.competition.name
          })
        }

        ws.sendCommand('soccer', 'getMatches', { matches: filteredMatches })
      })
      .catch((error) => {
        console.error(error)
      })
  })

  registerCommand('soccer', 'setMatch', (_, data) => {
    log.info(`Set soccer match to ${data.matchId}`)
    soccerData.matchId = data.matchId
    getMatchInfo(sendCommand)
  })

  registerCommand('soccer', 'getMatch', (ws) => {
    if (matchCache !== undefined) {
      ws.sendCommand('soccer', 'matchData', matchCache)
    }
  }, true)
}

module.exports.close = () => {
  soccerData.close()
  clearTimeout(getMatchInfoTimeout)
}
