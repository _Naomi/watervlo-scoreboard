const bcrypt = require('bcrypt')
const usersData = require('../utils/global_db.js').usersData
const teamsData = require('../utils/global_db.js').teamsData
const trainingsData = require('../utils/global_db.js').trainingsData
const log = require('../utils/logger.js').logger

function getNextId (object) {
  let id = 1

  while (Object.keys(object).includes(id.toString())) {
    id++
  }

  return id
}

function getFilteredUserData (username) {
  return (({ displayName, lastOnline, resetPassword }) => ({ displayName, lastOnline, resetPassword }))(usersData[username])
}

function sendUsersData (ws) {
  const users = {}
  for (const username of Object.keys(usersData)) {
    users[username] = getFilteredUserData(username)
  }
  ws.sendCommand('admin', 'getUsers', { users })
}

function sendTeamsData (ws) {
  ws.sendCommand('admin', 'getTeams', teamsData)
}

function sendTrainingsData (ws) {
  ws.sendCommand('admin', 'getTrainings', trainingsData)
}

module.exports.registerCommands = (registerCommand, sendCommand) => {
  registerCommand('admin', 'getUsers', (ws) => {
    sendUsersData(ws)
  })

  registerCommand('admin', 'getUser', (ws, data) => {
    log.info(`Getting data of user ${data.username}`)

    if (!data.username) {
      log.info('No username was given.')
      return
    }

    if (!Object.keys(usersData).includes(data.username)) {
      log.info(`User ${data.username} was not found`)
      return
    }

    const user = getFilteredUserData(data.username)
    user.username = data.username

    log.info(`Send the data of ${data.username}`)
    ws.sendCommand('admin', 'getUser', user)
  })

  registerCommand('admin', 'newUser', (ws, data) => {
    log.info('Registering new user.')
    if (!data.username || !data.displayName || !data.password) {
      log.info('No username and/or display name and/or password was given when creative a new user.')
      return
    }

    if (Object.keys(usersData).includes(data.username)) {
      log.warn(`The username ${data.username} already exists.`)
      return
    }

    bcrypt.hash(data.password, 10, (err, hash) => {
      if (err) {
        log.error(`Error while hashing password: ${err}`)
        return
      }
      usersData[data.username] = {
        displayName: data.displayName,
        password: hash,
        resetPassword: true,
        lastOnline: 0
      }

      log.info(`Successfully registered new user named ${data.username}`)

      sendUsersData(ws)
    })
  })

  registerCommand('admin', 'editUser', (ws, data) => {
    log.info('Editing user.')

    if (!data.username) {
      log.info('No username was given when editing a user.')
      return
    }

    if (!usersData[data.username]) {
      log.info('The user to be edited doesn\'t exists.')
      return
    }

    // Only change when the values exist in both objects.
    for (const key of Object.keys(data)) {
      if (key in usersData[data.username]) {
        usersData[data.username][key] = data[key]
      }
    }

    // Change password if exists.
    if (data.password) {
      bcrypt.hash(data.password, 10, (err, hash) => {
        if (err) {
          log.error(`Error while hashing password: ${err}`)
          return
        }
        usersData[data.username].password = hash

        log.info(`Successfully edited the user named ${data.username}`)

        sendUsersData(ws)
      })
    } else {
      log.info(`Successfully edited the user named ${data.username}`)

      sendUsersData(ws)
    }
  })

  registerCommand('admin', 'removeUser', (ws, data) => {
    log.info(`Removing user ${data.username}`)

    if (!usersData[data.username]) {
      log.info(`The user ${data.username} doesn't exist.`)
      return
    }

    delete usersData[data.username]
    log.warn(`The user ${data.username} was deleted.`)

    sendUsersData(ws)
  })

  registerCommand('admin', 'getTeams', (ws) => {
    sendTeamsData(ws)
  })

  registerCommand('admin', 'getTeam', (ws, data) => {
    log.info('Getting team data.')

    if (!data.id) {
      log.info('No team ID was given.')
      return
    }

    if (!teamsData[data.id]) {
      log.info(`No team exist with the ID ${data.id}`)
      return
    }

    const team = teamsData[data.id]
    team.id = data.id

    log.info(`Sending data of team with ID ${team.id}`)
    ws.sendCommand('admin', 'getTeam', team)
  })

  registerCommand('admin', 'newTeam', (ws, data) => {
    log.info('Adding new team.')
    const id = getNextId(teamsData)
    teamsData[id] = { name: data.name, shortName: data.shortName }
    sendTeamsData(ws)
  })

  registerCommand('admin', 'editTeam', (ws, data) => {
    log.info('Editing team.')

    if (!data.id) {
      log.info('No id was given when editing a team.')
      return
    }

    if (!teamsData[data.id]) {
      log.info('The team to be edited doesn\'t exists.')
      return
    }

    // Only change when the values exist in both objects.
    for (const key of Object.keys(data)) {
      if (key in teamsData[data.id]) {
        teamsData[data.id][key] = data[key]
      }
    }

    log.info(`Successfully edited the team named ${data.name}`)
    sendTeamsData(ws)
  })

  registerCommand('admin', 'removeTeam', (ws, data) => {
    log.info(`Removing team with id ${data.id}`)

    if (!teamsData[data.id]) {
      log.info(`There is no team with id ${data.id}.`)
      return
    }

    delete teamsData[data.id]
    log.warn(`The team with id ${data.id} was deleted.`)

    sendTeamsData(ws)
  })
}

module.exports.close = () => {
}
