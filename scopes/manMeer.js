const path = require('path')
const JsonDB = require('../utils/json_db.js')
const Timer = require('../utils/timer.js')

const manMeerData = new JsonDB(path.join(__dirname, '../data/manMeer.json'), {
  state: 'config',
  games: 5,
  home: [],
  away: [],
  mode: 'sequential',
  startingTeam: 'home'
}, 600)

function otherTeam (team) {
  return team === 'home' ? 'away' : 'home'
}

function calculateCurrentTeam () {
  if (manMeerData.mode === 'sequential') {
    return manMeerData[manMeerData.startingTeam].length < manMeerData.games ? manMeerData.startingTeam : otherTeam(manMeerData.startingTeam)
  } else if (manMeerData.mode === 'alternately') {
    return manMeerData.home.length === manMeerData.away.length ? manMeerData.startingTeam : otherTeam(manMeerData.startingTeam)
  }
}

module.exports.registerCommands = (registerCommand, sendCommand) => {
  function updateScore (score) {
    manMeerData[calculateCurrentTeam()].push(score)
    if (manMeerData.home.length >= manMeerData.games && manMeerData.away.length >= manMeerData.games) {
      manMeerData.state = 'finished'
    }
    sendCommand('manMeer', 'data', manMeerData)

    shotclockTimer.stop()
    shotclockTimer.setTime(20)
    sendCommand('manMeer', 'time', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  }

  function miss () {
    updateScore(false)
  }

  function hit () {
    updateScore(true)
  }

  const shotclockTimer = new Timer(100, 20, false, 'down', () => {
    sendCommand('manMeer', 'shotclockEnded')
  })

  registerCommand('manMeer', 'data', (ws) => {
    ws.sendCommand('manMeer', 'data', manMeerData)
  }, true)

  registerCommand('manMeer', 'gamesPlus', () => {
    manMeerData.games++
    sendCommand('manMeer', 'data', manMeerData)
  })

  registerCommand('manMeer', 'gamesMin', () => {
    if (manMeerData.games > 1) {
      manMeerData.games--
      sendCommand('manMeer', 'data', manMeerData)
    }
  })

  registerCommand('manMeer', 'toggleStartingTeam', () => {
    manMeerData.startingTeam = otherTeam(manMeerData.startingTeam)
    sendCommand('manMeer', 'data', manMeerData)
  })

  registerCommand('manMeer', 'setMode', (_, data) => {
    const modes = ['sequential', 'alternately']

    if (modes.includes(data.mode)) {
      manMeerData.mode = data.mode
      sendCommand('manMeer', 'data', manMeerData)
    }
  })

  registerCommand('manMeer', 'start', () => {
    manMeerData.state = 'active'
    sendCommand('manMeer', 'data', manMeerData)
  })

  registerCommand('manMeer', 'hit', () => {
    hit()
  })

  registerCommand('manMeer', 'miss', () => {
    miss()
  })

  registerCommand('manMeer', 'undo', () => {
    if (manMeerData.mode === 'sequential') {
      if (manMeerData[otherTeam(manMeerData.startingTeam)].length > 0) {
        manMeerData[otherTeam(manMeerData.startingTeam)].pop()
      } else {
        manMeerData[manMeerData.startingTeam].pop()
      }
    } else if (manMeerData.mode === 'alternately') {
      if (manMeerData.home.length === manMeerData.away.length) {
        manMeerData[otherTeam(manMeerData.startingTeam)].pop()
      } else {
        manMeerData[manMeerData.startingTeam].pop()
      }
    }

    if (manMeerData.home.length < manMeerData.games || manMeerData.away.length < manMeerData.games) {
      manMeerData.state = 'active'
    }
    sendCommand('manMeer', 'data', manMeerData)
  })

  registerCommand('manMeer', 'getTime', (ws) => {
    ws.sendCommand('manMeer', 'time', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  }, true)

  registerCommand('manMeer', 'toggleTime', () => {
    if (shotclockTimer.isRunning) {
      shotclockTimer.stop()
    } else {
      shotclockTimer.start()
    }
    sendCommand('manMeer', 'time', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  })

  registerCommand('manMeer', 'reset20', () => {
    shotclockTimer.setTime(20)
    sendCommand('manMeer', 'time', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  })

  registerCommand('manMeer', 'resetAll', () => {
    manMeerData.home = []
    manMeerData.away = []
    manMeerData.state = 'config'
    sendCommand('manMeer', 'data', manMeerData)

    shotclockTimer.stop()
    shotclockTimer.setTime(20)
    sendCommand('manMeer', 'time', { time: shotclockTimer.time, running: shotclockTimer.isRunning })
  })
}

module.exports.close = () => {
  manMeerData.close()
}
