require('dotenv').config()

const path = require('path')
const JsonDB = require('../utils/json_db.js')
const axios = require('axios')

const startData = new JsonDB(path.join(__dirname, '../data/start.json'), {
  customText: '',
  customTextAlignment: 'center',
  latestWaterpoloMatches: '',
  sponsorsEnabled: false,
  sponsorData: []
}, 600)

module.exports.registerCommands = (registerCommand, sendCommand) => {
  registerCommand('start', 'data', (ws) => {
    ws.sendCommand('start', 'data', startData)
  }, true)

  registerCommand('start', 'setCustomText', (ws, data) => {
    startData.customText = data.text
    sendCommand('start', 'data', startData)
  })

  registerCommand('start', 'setCustomTextAlignment', (ws, data) => {
    startData.customTextAlignment = data.alignment
    sendCommand('start', 'data', startData)
  })

  registerCommand('start', 'setSponsorsEnabled', (ws, data) => {
    startData.sponsorsEnabled = data.sponsorsEnabled
    sendCommand('start', 'data', startData)
  })
}

async function getSponsors () {
  // Clear existing data
  while (startData.sponsorData.length > 0) {
    startData.sponsorData.pop()
  }

  const teams = process.env.SPONSOR_TEAMS.split(',')
  for (const team of teams) {
    const response = await axios.get(`https://data.sportlink.com/team-sponsors?clientId=${process.env.SPORTLINK_WATERVLO_ID}&teamcode=-1&lokaleteamcode=${team}`)
    for (const sponsor of response.data) {
      startData.sponsorData.push(sponsor)
    }
  }
}

function getLatestWaterpoloMatches () {
  axios.get(`https://data.sportlink.com/uitslagen?clientId=${process.env.SPORTLINK_WATERVLO_ID}&aantaldagen=-7&weekoffset=0`)
    .then((response) => {
      let string = '<b>Uitslagen:</b> '
      const scoreRegex = RegExp(/([0-9]*) - ([0-9]*)/)
      const matches = response.data

      for (const [index, match] of matches.entries()) {
        const score = scoreRegex.exec(match.uitslag)

        if (score !== null) {
          let homeTeam, homeScore, awayTeam, awayScore

          if (match.thuisteam.includes('Watervlo')) { // Home is Watervlo
            homeTeam = `<b>${match.thuisteam}</b>`
            homeScore = `<b>${score[1]}</b>`
          } else { // Home is not Watervlo
            homeTeam = match.thuisteam
            homeScore = score[1]
          }
          if (match.uitteam.includes('Watervlo')) { // Away is Watervlo
            awayTeam = `<b>${match.uitteam}</b>`
            awayScore = `<b>${score[2]}</b>`
          } else { // Away is not Watervlo
            awayTeam = match.uitteam
            awayScore = score[2]
          }
          string += `${homeTeam} - ${awayTeam}: ${homeScore} - ${awayScore}${index < matches.length - 1 ? '   🤽   ' : ''}`
        }
      }

      if (string === '<b>Uitslagen:</b> ') {
        string += 'Geen afgelopen week'
      }
      startData.latestWaterpoloMatches = string
    })
    .catch((error) => {
      console.error(error)
    })
}

getSponsors()
const getSponsorsInterval = setInterval(getSponsors, 24 * 60 * 60 * 1000)

getLatestWaterpoloMatches()
const getLatestWaterpoloMatchesInterval = setInterval(getLatestWaterpoloMatches, 600000)

module.exports.close = () => {
  startData.close()
  clearInterval(getLatestWaterpoloMatchesInterval)
  clearInterval(getSponsorsInterval)
}
